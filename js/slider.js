$(document).ready(function () {
    $(".banner-image-slider").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      arrows: true,
      draggable: false,
      prevArrow: `<button type='button' class='slick-prev slick-arrow'><img src="./images/prev-btn.png" alt=""></button>`,
      nextArrow: `<button type='button' class='slick-next slick-arrow'><img src="./images/next-btn.png" alt=""></button>`,
      dots: true,
      responsive: [
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 1,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            arrows: false,
            infinite: false,
          },
        },
      ],
      // autoplay: true,
      // autoplaySpeed: 1000,
    });
    $(".image-slider-roadmap").slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      infinite: true,
      arrows: true,
      draggable: false,
      prevArrow: `<button type='button' class='slick-prev slick-arrow'><img src="./images/prev-roadmap.png" alt=""></button>`,
      nextArrow: `<button type='button' class='slick-next slick-arrow'><img src="./images/next-roadmap.png" alt=""></button>`,
      dots: false,
      responsive: [
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 3,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            arrows: false,
            infinite: false,
          },
        },
      ],
      // autoplay: true,
      // autoplaySpeed: 1000,
    });
    $(".hero-image-slider").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      arrows: true,
      draggable: false,
      prevArrow: `<button type='button' class='slick-prev slick-arrow'><img src="./images/prev-btn.png" alt=""></button>`,
      nextArrow: `<button type='button' class='slick-next slick-arrow'><img src="./images/next-btn.png" alt=""></button>`,
    //   dots: true,
      responsive: [
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 1,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            arrows: false,
            infinite: false,
          },
        },
      ],
      // autoplay: true,
      // autoplaySpeed: 1000,
    });
    $('.hero-image-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        document.getElementsByClassName(`dot-${currentSlide}`)[0].setAttribute('src', './images/hero-tribe.png')
        document.getElementsByClassName(`dot-${nextSlide}`)[0].setAttribute('src', './images/hero-tribe-active.png')
      });
    
    $('.customer-logos').slick({
      slidesToShow: 6,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 1500,
      arrows: false,
      dots: false,
      pauseOnHover: false,
      responsive: [{
          breakpoint: 768,
          settings: {
              slidesToShow: 4
          }
      }, {
          breakpoint: 520,
          settings: {
              slidesToShow: 3
          }
      }]
    });
    });
    function gotoIndex(index) {
        $('.hero-image-slider').slick('slickGoTo', index);
    }
  